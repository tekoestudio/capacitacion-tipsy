<?php

require_once __DIR__ . '/../vendor/autoload.php';

//Dependencias de clases
use Tipsy\Tipsy;
use Nette\Mail\Message;
use Nette\Mail\SendmailMailer;

//Tipsy
Tipsy::config(__DIR__ .'/../config/*.ini');

//Variables Globales
global $tekodb, $tekoauth, $titulo, $site_url;

//Cargar configuración de la Base de datos
$db_user = Tipsy::config()['db']['user'];
$db_pass = Tipsy::config()['db']['pass'];
$db_name = Tipsy::config()['db']['name'];
$db_host = Tipsy::config()['db']['host'];
$tekodb = new ezSQL_mysqli($db_user, $db_pass, $db_name, $db_host);

//Url del sitio
$site_url = Tipsy::config()['sitio']['url'];

//PDO driver para conexión a MySQL
$pdo = new PDO("mysql:dbname={$db_name};host={$db_host};charset=utf8mb4", $db_user, $db_pass);
//Revisar si existen las tablas necesarias para usuarios en la DB, si no, crearlas
$existe = $tekodb->get_col("SHOW TABLES LIKE 'users'");
if(!count($existe)){
    $sql = file_get_contents(__DIR__ . '/../vendor/delight-im/auth/Database/MySQL.sql');
    $pdo->exec($sql);
}
//Configurar variable global de autentificación (Docs: https://github.com/delight-im/PHP-Auth)
$tekoauth = new \Delight\Auth\Auth($pdo);

//Funciones de TekoCore
require_once('teko-core/auth.php');
require_once('teko-core/generales.php');
require_once('teko-core/mail.php');

//Funciones Globales
require_once('funciones.php');

//Rutas
$rutas = Tipsy::router();
require_once('rutas.php');

//Inicializar App
Tipsy::run();
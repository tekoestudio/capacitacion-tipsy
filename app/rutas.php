<?php

/*
 En este archivo se definirán las rutas de la aplicación.
*/

$rutas->home(function($View, $Params, $Scope){
    global $titulo;
    $titulo = 'Inicio';
    $View->display('index');
});

$rutas->otherwise(function($View){
    global $titulo;
    $titulo = '404';
    $View->display('404');
});
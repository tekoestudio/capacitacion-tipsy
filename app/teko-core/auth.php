<?php

//Registrar
function tekoauth_register($correo, $pass, $role = 'usuario' ,$usuario = null, $verifica = false, $extra = []){
    global $tekodb, $tekoauth, $token;
    try {
        $token = '';
        $id = ($verifica) ? $tekoauth->register($correo, $pass, $usuario, function ($selector, $token2) {
            global $token;
            $token = urlencode(base64_encode(json_encode(compact('selector', 'token2'))));
        }) : $tekoauth->register($correo, $pass, $usuario);
        if(count($extra)){
            $extra['role'] = $role;
            $tekodb->update('users', $extra, compact('id'));
        } else {
            $tekodb->update('users', compact('role'), compact('id'));
        }
        return array('error' => FALSE, 'id'=>$id, 'token' => $token);
    }
    catch (\Delight\Auth\InvalidEmailException $e) {
        return array('error' => TRUE, 'tipo' => 'correo');
    }
    catch (\Delight\Auth\InvalidPasswordException $e) {
        return array('error' => TRUE, 'tipo' => 'contraseña');
    }
    catch (\Delight\Auth\UserAlreadyExistsException $e) {
        return array('error' => TRUE, 'tipo' => 'usuario_existente');
    }
    catch (\Delight\Auth\TooManyRequestsException $e) {
        return array('error' => TRUE, 'tipo' => 'intentos_multiples');
    }
    catch (\Delight\Auth\DatabaseError $e){
        return array('error' => TRUE, 'tipo' => 'db');
    }
}

//Confirmar Email
function tekoauth_confirm($token){
    global $tekoauth;
    try {
        $partes = json_decode(base64_decode(urldecode($token)));
        $tekoauth->confirmEmail($partes->selector, $partes->token);
        return array('error' => FALSE);
    }
    catch (\Delight\Auth\InvalidSelectorTokenPairException $e) {
        return array('error' => TRUE, 'tipo' => 'token_invalido');
    }
    catch (\Delight\Auth\TokenExpiredException $e) {
        return array('error' => TRUE, 'tipo' => 'token_expirado');
    }
    catch (\Delight\Auth\TooManyRequestsException $e) {
        return array('error' => TRUE, 'tipo' => 'intentos_multiples');
    }
}

//Contraseña Perdida
function tekoauth_recover($correo){
    global $tekoauth;
    try {
        $token = '';
        $tekoauth->forgotPassword($correo, function ($selector, $token) {
            $token = urlencode(base64_encode(json_encode(compact('selector', 'token'))));
        });
        return array('error' => FALSE, 'token' => $token);
    }
    catch (\Delight\Auth\InvalidEmailException $e) {
        return array('error' => TRUE, 'tipo' => 'correo');
    }
    catch (\Delight\Auth\TooManyRequestsException $e) {
        return array('error' => TRUE, 'tipo' => 'intentos_multiples');
    }
}

//Resetear Contraseña
function tekoauth_reset_password($token, $contraseña){
    global $tekoauth;
    try {
        $partes = json_decode(base64_decode(urldecode($token)));
        $tekoauth->resetPassword($partes->selector, $partes->token, $contraseña);
        return array('error' => FALSE);
    }
    catch (\Delight\Auth\InvalidSelectorTokenPairException $e) {
        return array('error' => TRUE, 'tipo' => 'token_invalido');
    }
    catch (\Delight\Auth\TokenExpiredException $e) {
        return array('error' => TRUE, 'tipo' => 'token_expirado');
    }
    catch (\Delight\Auth\InvalidPasswordException $e) {
        return array('error' => TRUE, 'tipo' => 'contraseña');
    }
    catch (\Delight\Auth\TooManyRequestsException $e) {
        return array('error' => TRUE, 'tipo' => 'intentos_multiples');
    }
}

//Cambiar contraseña del usuario actual
function tekoauth_change_password($antigua, $nueva){
    try {
        $auth->changePassword($antigua, $nueva);
        return array('error' => FALSE);
    }
    catch (\Delight\Auth\NotLoggedInException $e) {
        return array('error' => TRUE, 'tipo' => 'sin_ingresar');
    }
    catch (\Delight\Auth\InvalidPasswordException $e) {
        return array('error' => TRUE, 'tipo' => 'contraseña');
    }
}

//Cambiar role de usuario
function tekoauth_change_role($id, $role){
    global $tekodb;
    $tekodb->update('users', compact('role'), compact('id'));
    return array('error' => FALSE);
}

//Ingresar
function tekoauth_login($correo, $pass, $recordar = FALSE){
    global $tekoauth, $tekodb;
    $user = $tekodb->get_row("SELECT * FROM users WHERE username='{$correo}'");
    $correo = (!empty($user)) ? $user->email : $correo;
    try {
        $tekoauth->login($correo, $pass, $recordar);
        return array('error' => FALSE, 'id'=>$tekoauth->getUserId());
    }
    catch (\Delight\Auth\InvalidEmailException $e) {
        return array('error' => TRUE, 'tipo' => 'correo');
    }
    catch (\Delight\Auth\InvalidPasswordException $e) {
        return array('error' => TRUE, 'tipo' => 'contraseña');
    }
    catch (\Delight\Auth\EmailNotVerifiedException $e) {
        return array('error' => TRUE, 'tipo' => 'correo_no_verificado');
    }
    catch (\Delight\Auth\TooManyRequestsException $e) {
        return array('error' => TRUE, 'tipo' => 'intentos_multiples');
    }
}

//Cerrar Sesión
function tekoauth_logout(){
    global $tekoauth;
    $tekoauth->logout();
    return array('error' => FALSE);
}

//Obtener id del usuario actual
function tekoauth_id(){
    global $tekoauth;
    if ($tekoauth->isLoggedIn()) {
        return $tekoauth->getUserId();
    }
    else {
        return 0;
    }
}
//Alias
function get_current_user_id(){ return tekoauth_id(); }

//Obtener el row del usuario actual
function tekoauth_current(){
    global $tekoauth,$tekodb;
    $id = tekoauth_id();
    return $tekodb->get_row("SELECT * FROM users WHERE id={$id}");
}


//Obtener el rol del usuario actual
function get_current_user_role(){
    global $tekodb;
    return $tekodb->get_var("SELECT role FROM users WHERE id=".tekoauth_id());
}